﻿using Ecom.CategoryService.Models;
using Ecom.CustomerService.Commands;
using Ecom.CustomerService.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Ecom.CustomerService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IMediator mediator;

        public CustomerController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("add-new-customer")]
        public async Task<IActionResult> AddnewCustomer([FromBody] EcomCustomers newCustomer)
        {
            var data = await mediator.Send(new AddNewCustomerCommand() { Customer = newCustomer });
            return Ok(data);

        }
        [HttpGet]
        [Route("get-customer-by-id/{id}")]
        public async Task<IActionResult> GetCustomerById(int id)
        {
            var data = await mediator.Send(new GetByIdQuery() { Id = id });
            return Ok(data);

        }
    }
}

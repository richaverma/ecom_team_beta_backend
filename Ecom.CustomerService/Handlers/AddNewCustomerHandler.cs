﻿using Ecom.CategoryService.Models;
using Ecom.CustomerService.BusinessLayer.Interface;
using Ecom.CustomerService.Commands;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.CustomerService.Handlers
{
    public class AddNewCustomerHandler : IRequestHandler<AddNewCustomerCommand, EcomCustomers>
    {
        ICustomer _data;

        public AddNewCustomerHandler(ICustomer data)
        {
            _data = data;
        }

        public async Task<EcomCustomers> Handle(AddNewCustomerCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.AddNewCustomer(request.Customer));
        }
    }
}

﻿using Ecom.CategoryService.Models;
using MediatR;

namespace Ecom.CustomerService.Queries
{
    public class GetByIdQuery:IRequest<EcomCustomers>
    {
        public int Id { get; set; }
    }
}

﻿using Ecom.PaymentService.BusinessLayer.Interface;
using Ecom.PaymentService.Commands;
using Ecom.PaymentService.Handlers;
using Ecom.PaymentService.Models;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Payment.Microservice.Test.Commands
{
    public class MakePaymentHandlerTest
    {
        private readonly EcomPayment _payment;
        private readonly Mock<IPayment> _mockrepo;

        public MakePaymentHandlerTest()
        {
            _mockrepo = Mocks.MockRepository.GetPaymentService();
            _payment = new EcomPayment
            {
                OrderId = 700,
                CustomerId = 3,
                PaymentMode = "Card",
                CardNumber = "12234",
                CardCvv = 113,
                CardExpiry = DateTime.Now,
                CardName = "Richa"
            };
        }
        [Fact]
        public void PaymentHandlerTest()
        {
            var handler = new MakePaymentHandler(_mockrepo.Object);
            var result = handler.Handle(new MakePaymentCommand { Payment=_payment}, CancellationToken.None);
            //Assert
             result.ShouldBeOfType<Task<EcomPayment>>();
            
        }
    }
}

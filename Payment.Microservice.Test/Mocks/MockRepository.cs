﻿using Ecom.PaymentService.BusinessLayer.Interface;
using Ecom.PaymentService.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Payment.Microservice.Test.Mocks
{
    public class MockRepository
    {
       
        //Creating a Mock Method for setting up mock behaviour of interface's methods
        public static Mock<IPayment> GetPaymentService()
        {
           
            //Creating mock object of type IPayment interface
            var mockrepo = new Mock<IPayment>();
     //Defining method of interface for testing but return type and and parameter type of the method should not be modified
            mockrepo.Setup(o => o.MakePayment(It.IsAny<EcomPayment>())).Returns((EcomPayment payment)=>
            {
                return payment;
            }
            );
            
            return mockrepo;
        }

    }
}

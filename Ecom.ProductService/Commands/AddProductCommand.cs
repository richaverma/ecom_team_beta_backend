﻿using Ecom.ProductService.Models;
using MediatR;
using System.Collections.Generic;

namespace Ecom.ProductService.Commands
{
    public class AddProductCommand:IRequest<IEnumerable<EcomProducts>>
    {
        public EcomProducts EcomProducts { get; set; }
    }
}

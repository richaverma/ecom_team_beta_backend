﻿using Ecom.ProductService.BusinessLayer.Interface;
using Ecom.ProductService.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecom.ProductService.BusinessLayer.Services
{
   
    public class ProductServices : IProducts
    {
        private EcomContext _context;

        public ProductServices(EcomContext ecomContext)
        {
            _context = ecomContext;
        }
        public IEnumerable<EcomProducts> AddProduct(EcomProducts newProduct)
        {
            var x = _context.EcomProducts.Add(newProduct);
            _context.SaveChanges();
            return _context.EcomProducts.ToList();
        }

        public IEnumerable<EcomProducts> DeleteProduct(EcomProducts deleteProduct)
        {
            _context.EcomProducts.Remove(deleteProduct);
            _context.SaveChanges();
            return _context.EcomProducts.ToList();
        }
        

        public EcomProducts EditProduct(EcomProducts newProduct)
        {
            _context.EcomProducts.Update(newProduct);
            _context.SaveChanges();
            return newProduct;

        }

        public IEnumerable<EcomProducts> GetAllProducts()
        {
            return _context.EcomProducts.ToList();
        }

        public EcomProducts GetProductbyId(int id)
        {
            return _context.EcomProducts.SingleOrDefault(x => x.ProductId == id);
        }

        public IEnumerable<EcomProducts> GetProductsByCategoryId(int id)
        {
            var data = _context.EcomProducts.Where(x => x.CategoryId == id);
            return data.ToList();
        }
    }
}

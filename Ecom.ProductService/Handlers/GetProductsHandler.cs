﻿using Ecom.ProductService.BusinessLayer.Interface;
using Ecom.ProductService.Models;
using Ecom.ProductService.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.ProductService.Handlers
{
    public class GetProductsHandler : IRequestHandler<GetProductsQuery, IEnumerable<EcomProducts>>
    {
        IProducts _data;

        public GetProductsHandler(IProducts data)
        {
            _data = data;
        }

        public async Task<IEnumerable<EcomProducts>> Handle(GetProductsQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetAllProducts());
        }
    }
}

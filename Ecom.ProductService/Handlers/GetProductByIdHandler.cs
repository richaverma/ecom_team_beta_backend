﻿using Ecom.ProductService.BusinessLayer.Interface;
using Ecom.ProductService.Models;
using Ecom.ProductService.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.ProductService.Handlers
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQuery, EcomProducts>
    {
        IProducts _data;

        public GetProductByIdHandler(IProducts data)
        {
            _data = data;
        }
        public async Task<EcomProducts> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetProductbyId(request.Id));
        }
    }
}

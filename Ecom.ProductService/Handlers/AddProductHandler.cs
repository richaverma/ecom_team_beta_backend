﻿using Ecom.ProductService.BusinessLayer.Interface;
using Ecom.ProductService.Commands;
using Ecom.ProductService.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.ProductService.Handlers
{
    public class AddProductHandler : IRequestHandler<AddProductCommand, IEnumerable<EcomProducts>>
    {
        IProducts _data;

        public AddProductHandler(IProducts data)
        {
            _data = data;
        }
        public async Task<IEnumerable<EcomProducts>> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.AddProduct(request.EcomProducts));
        }
    }
}

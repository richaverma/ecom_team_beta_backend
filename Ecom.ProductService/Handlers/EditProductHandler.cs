﻿using Ecom.ProductService.BusinessLayer.Interface;
using Ecom.ProductService.Commands;
using Ecom.ProductService.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.ProductService.Handlers
{
    public class EditProductHandler : IRequestHandler<EditProductCommand, EcomProducts>
    {
        IProducts _data;

        public EditProductHandler(IProducts data)
        {
            _data = data;
        }

        public async Task<EcomProducts> Handle(EditProductCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.EditProduct(request.EcomProducts));
        }
    }
}

﻿using Ecom.ProductService.Models;
using MediatR;

namespace Ecom.ProductService.Queries
{
    public class GetProductByIdQuery:IRequest<EcomProducts>
    {
        public int Id { get; set; }
    }
}

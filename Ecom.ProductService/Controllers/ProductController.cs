﻿using Ecom.ProductService.Commands;
using Ecom.ProductService.Models;
using Ecom.ProductService.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Ecom.ProductService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMediator mediator;

        public ProductController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        [Route("get-all-products")]
        public async Task<IActionResult> GetAllProducts()
        {
            var data = await mediator.Send(new GetProductsQuery());
            return Ok(data);

        }


        [HttpPost]
        [Route("add-new-product")]
        public async Task<IActionResult> AddnewProduct([FromBody] EcomProducts newProduct)
        {
            var data = await mediator.Send(new AddProductCommand() { EcomProducts = newProduct });
            return Ok(data);

        }


        [HttpDelete]
        [Route("delete-product")]
        public async Task<IActionResult> DeleteProduct([FromBody] EcomProducts deleteProduct)
        {
            var data = await mediator.Send(new DeleteProductCommand() { EcomProducts = deleteProduct });
            return Ok(data);

        }

        [HttpPut]
        [Route("edit-product")]
        public async Task<IActionResult> EditProduct([FromBody] EcomProducts EditProduct)
        {
            var data = await mediator.Send(new EditProductCommand() { EcomProducts = EditProduct });
            return Ok(data);

        }
        [HttpGet]
        [Route("get-product-by-id/{id}")]
        public async Task<IActionResult> GetProductById(int id)
        {
            var data = await mediator.Send(new GetProductByIdQuery() { Id = id });
            return Ok(data);

        }

        [HttpGet]
        [Route("get-product-by-cat-id/{id}")]
        public async Task<IActionResult> GetProductByCatId(int id)
        {
            var data = await mediator.Send(new GetProductByCategoryIdQuery() { Id = id });
            return Ok(data);

        }


    }
}

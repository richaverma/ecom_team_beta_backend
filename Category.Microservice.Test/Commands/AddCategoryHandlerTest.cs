﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using Ecom.CategoryService.BusinessLayer.Interface;
using Moq;
using Ecom.CategoryService.Models;
using Ecom.CategoryService.Handlers;
using Ecom.CategoryService.Commands;
using Shouldly;
using System.Linq;

namespace Category.MicroService.Test.Commands
{
    public class AddCategoryHandlerTest
    {
        private readonly Mock<ICategory> _mockRepo;
        private readonly EcomCategory _category;

        public AddCategoryHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetCategoryService();

            _category = new EcomCategory
            {
                CategoryId = 506,
                CategoryName = "Stationary"
            };
        }

        [Fact]
        public async Task AddCategoryTest()
        {//creating object of corresponding handler
            var handler = new AddCategoryHandler(_mockRepo.Object);
            //calling  handle method of the handler through the handler object
            var result = handler.Handle(new AddNewCategoryCommand { EcomCategory = _category }, CancellationToken.None);
            //Assert
            var category =  _mockRepo.Object.GetAllCategory();

            await result.ShouldBeOfType<Task<IEnumerator<EcomCategory>>>();

            category.Count().ShouldBe(3);
        }
    }
}


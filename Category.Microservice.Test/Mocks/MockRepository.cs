﻿using Ecom.CategoryService.BusinessLayer.Interface;
using Ecom.CategoryService.BusinessLayer.Services;
using Ecom.CategoryService.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Category.MicroService.Test.Mocks
{
    public static class MockRepository
    {
        public static Mock<ICategory> GetCategoryService()
        {
            var catlist = new List<EcomCategory>
            {
                new EcomCategory
                {
                    CategoryId = 300,
                    CategoryName = "Dress"
                },
                new EcomCategory
                {
                    CategoryId = 301,
                    CategoryName = "Electronics"
                }
            };

            var mockrepo = new Mock<ICategory>();

            mockrepo.Setup(r => r.GetAllCategory()).Returns(catlist);

            mockrepo.Setup(r => r.AddNewCategory(It.IsAny<EcomCategory>())).Returns((EcomCategory category) =>
            {
                catlist.Add(category);
                return catlist;
            });

            return mockrepo;
        }
    }
}

   

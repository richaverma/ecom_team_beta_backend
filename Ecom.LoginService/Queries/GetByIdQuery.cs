﻿using Ecom.LoginService.Models;
using MediatR;

namespace Ecom.LoginService.Queries
{
    public class GetByIdQuery:IRequest<EcomLogin>
    {
        public int Id { get; set; }
    }
}

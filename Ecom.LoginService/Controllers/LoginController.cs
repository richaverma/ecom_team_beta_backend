﻿using Ecom.LoginService.Commands;
using Ecom.LoginService.Models;
using Ecom.LoginService.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Ecom.LoginService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController: ControllerBase
    {

        private readonly IMediator mediator;

        public LoginController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        [Route("get-login-id/{id}")]
        public async Task<IActionResult> GetLoginById(int id)
        {

            var data = await this.mediator.Send(new GetByIdQuery() { Id = id });
            return Ok(data);

        }

        [HttpGet]
        [Route("get-login-details/{email}")]
        public async Task<IActionResult> GetLoginByCustomerEmail(string email)
        {

            var data = await this.mediator.Send(new GetByCustomerEmail() { Email = email });
            return Ok(data);
        }

        [HttpPost]
        [Route("new-login")]
        public async Task<IActionResult> NewLogin(EcomLogin newLogin)
        {
            var data = await this.mediator.Send(new NewLoginCommand() { newLogin = newLogin });
            return Ok(data);
        }

    }
}

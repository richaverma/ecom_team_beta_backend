﻿using Ecom.LoginService.BusinessLayer.Interface;
using Ecom.LoginService.Models;
using Ecom.LoginService.Queries;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.LoginService.Handlers
{
    public class GetByIdHandler : IRequestHandler<GetByIdQuery, EcomLogin>
    {
        ILogin _data;

        public GetByIdHandler(ILogin data)
        {
            _data = data;
        }
        public async Task<EcomLogin> Handle(GetByIdQuery request, CancellationToken cancellationToken)
        {
            return await await Task.FromResult(_data.GetLoginDetailsById(request.Id));
        }
    }
}

﻿using Ecom.LoginService.BusinessLayer.Interface;
using Ecom.LoginService.Models;
using Ecom.LoginService.Queries;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.LoginService.Handlers
{
    public class GetByCustomerEmailHandler : IRequestHandler<GetByCustomerEmail, EcomLogin>
    {
        ILogin _data;

        public GetByCustomerEmailHandler(ILogin data)
        {
            _data = data;
        }
        public async Task<EcomLogin> Handle(GetByCustomerEmail request, CancellationToken cancellationToken)
        {
            return await await Task.FromResult(_data.GetLoginDetailsByCustomerEmail(request.Email));
        }
    }
}

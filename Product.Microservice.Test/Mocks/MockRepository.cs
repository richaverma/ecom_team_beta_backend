﻿using Ecom.ProductService.BusinessLayer.Interface;
using Ecom.ProductService.Models;
using Microsoft.IdentityModel.Tokens;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Product.Microservice.Test.Mocks
{
    public class MockRepository
    {
        public static Mock<IProducts> GetProductsService()
        {
            var mockrepo = new Mock<IProducts>();
            var productList = new List<EcomProducts>
            {
                new EcomProducts
                {
                    ProductId = 1,
                    CategoryId = 1,
                    ProductDescription = "Cool Shirts",
                    ProductName="Shirts",
                    ProductPrice = 200,
                    ProductType = "Clothing",
                    ProductImage = "https://random.url"
                },
                 new EcomProducts
                {
                    ProductId = 2,
                    CategoryId = 1,
                    ProductDescription = "Cool Pants",
                    ProductName="Pants",
                    ProductPrice = 200,
                    ProductType = "Clothing",
                    ProductImage = "https://random.url"
                }
            };
            mockrepo.Setup(x => x.DeleteProduct(It.IsAny<EcomProducts>())).Returns((EcomProducts product) =>
            {
                var deleteproduct = productList.SingleOrDefault(x=>x.ProductId == product.ProductId);
                productList.Remove(deleteproduct);
                return productList;
            });
            
            mockrepo.Setup(x=>x.GetAllProducts()).Returns(productList);
            mockrepo.Setup(x => x.GetProductbyId(It.IsAny<int>())).Returns((int id) =>
            {
                var product = productList.SingleOrDefault(x => x.ProductId == id);
                return product;
            });
            return mockrepo;
        }
    }
}

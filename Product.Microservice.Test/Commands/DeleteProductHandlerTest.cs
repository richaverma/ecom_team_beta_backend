﻿using Ecom.ProductService.BusinessLayer.Interface;
using Ecom.ProductService.Commands;
using Ecom.ProductService.Handlers;
using Ecom.ProductService.Models;
using Ecom.ProductService.Queries;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Product.Microservice.Test.Commands
{
    public class DeleteProductHandlerTest
    {
        private  EcomProducts _product;
        private readonly Mock<IProducts> mockrepo;

        public DeleteProductHandlerTest()
        {
            _product = new EcomProducts
            {
                ProductId = 1,
                CategoryId = 1,
                ProductDescription = "Cool Shirts",
                ProductName = "Shirts",
                ProductPrice = 200,
                ProductType = "Clothing",
                ProductImage = "https://random.url"
            };
            mockrepo = Mocks.MockRepository.GetProductsService();
        }

        [Fact]
        public async void DeleteProductTest()
        {
            var handler = new DeleteProductHandler(mockrepo.Object);
            var result = handler.Handle(new DeleteProductCommand { EcomProducts = _product}, CancellationToken.None);
           await result.ShouldBeOfType<Task<IEnumerable<EcomProducts>>>();
         //  var getProducthandler = new GetProductsHandler(mockrepo.Object);
           // var productList = getProducthandler.Handle(new GetProductsQuery { }, CancellationToken.None);
           // productList.ShouldBeOfType<IEnumerable<EcomProducts>>();
            var ProductsList = mockrepo.Object.GetAllProducts();
            ProductsList.Count().ShouldBe(0);
        }

       
    }
}

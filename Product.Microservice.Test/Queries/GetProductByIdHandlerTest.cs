﻿using Ecom.ProductService.BusinessLayer.Interface;
using Ecom.ProductService.Handlers;
using Ecom.ProductService.Models;
using Ecom.ProductService.Queries;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Product.Microservice.Test.Queries
{
    public class GetProductByIdHandlerTest
    {
        private readonly Mock<IProducts> mockrepo;

        public GetProductByIdHandlerTest()
        {
            mockrepo = Mocks.MockRepository.GetProductsService();
        }
        [Fact]
        public void GetProductByIdTest()
        {
            var handler = new GetProductByIdHandler(mockrepo.Object);
            int productId = 2;
            var result = handler.Handle(new GetProductByIdQuery { Id = productId } , CancellationToken.None);
            result.ShouldBeOfType<Task<EcomProducts>>();
            var product = mockrepo.Object.GetProductbyId(2);
            product.ProductId.ShouldBe(2);
        }
    }
}

﻿using Ecom.OrderService.BusinessLayer.Interface;
using Ecom.OrderService.BusinessLayer.Services;
using Ecom.OrderService.Models;
using System;

namespace Ecom.OrderService
{
    public class AddOrderRequestProcessor
    {
        private readonly IOrder _addorderRepositoryMock;



        public AddOrderRequestProcessor(IOrder _addorderRepository)
        {
            _addorderRepositoryMock = _addorderRepository;
        }



        public AddOrderResponse Add(AddOrderRequest request)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }



            //_addorderRepositoryMock.Save(Create<AddOrder>(request));



            return Create<AddOrderResponse>(request);
        }



        private static T Create<T>(AddOrderRequest request) where T : EcomOrders, new()
        {
            return new T
            {
                OrderId = request.OrderId,

            };
        }
    }
}

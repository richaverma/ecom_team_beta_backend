﻿using Ecom.OrderService.Models;
using MediatR;

namespace Ecom.OrderService.Commands
{
    public class PlaceOrderCommand:IRequest<EcomOrders>
    {
        public EcomOrders newOrder { get; set; }
    }
}

﻿using Ecom.OrderService.BusinessLayer.Interface;
using Ecom.OrderService.Models;
using Ecom.OrderService.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.OrderService.Handlers
{
    public class GetOrderByIdHandler : IRequestHandler<GetOrderByIdQuery, EcomOrders>
    {
        IOrder _data;

        public GetOrderByIdHandler(IOrder data)
        {
            _data = data;
        }
        public async Task<EcomOrders> Handle(GetOrderByIdQuery request, CancellationToken cancellationToken)
        {
            return await await Task.FromResult(_data.GetOrderById(request.Id));
        }
    }
}

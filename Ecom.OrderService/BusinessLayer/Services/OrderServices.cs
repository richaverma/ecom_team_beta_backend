﻿using Ecom.OrderService.BusinessLayer.Interface;
using Ecom.OrderService.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Ecom.OrderService.BusinessLayer.Services
{
    public class OrderServices : IOrder
    {
        private EcomContext _context;

        public OrderServices(EcomContext ecomContext)
        {
            _context = ecomContext;
        }

        public async Task<EcomOrders> GetOrderById(int id)
        {
            return await Task.FromResult(_context.EcomOrders.SingleOrDefault(x => x.CustomerId == id));
        }

        public EcomOrders PlaceOrder(EcomOrders newOrder)
        {
            _context.EcomOrders.Add(newOrder);
            _context.SaveChanges();
            return newOrder;
        }
    }
}

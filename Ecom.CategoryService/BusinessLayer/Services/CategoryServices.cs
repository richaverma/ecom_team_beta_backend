﻿using Ecom.CategoryService.BusinessLayer.Interface;
using Ecom.CategoryService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecom.CategoryService.BusinessLayer.Services
{
    public class CategoryServices : ICategory
    {
        private EcomContext _context;

        public CategoryServices(EcomContext ecomContext)
        {
            _context = ecomContext;
        }

        public IEnumerable<EcomCategory> AddNewCategory(EcomCategory newCategory)
        {
            /*
             Duplicate Category cant be added!!
            
            */

            var result = _context.EcomCategory.SingleOrDefault(x => x.CategoryName == newCategory.CategoryName);
            if(result == null)
            {
                _context.EcomCategory.Add(newCategory);
                _context.SaveChanges();
                return _context.EcomCategory.ToList();

            }
            else
            {
                
                return _context.EcomCategory.ToList();
            }
        }

        public EcomCategory EditById(int id, string newCategoryName)
        {
            EcomCategory category = _context.EcomCategory.Find(id);
            category.CategoryName = newCategoryName;
            _context.SaveChanges();
            return category;

        }

        public IEnumerable<EcomCategory> GetAllCategory()
        {
           
            return _context.EcomCategory.ToList();
            
        }
    }
}

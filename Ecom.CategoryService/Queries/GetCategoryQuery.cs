﻿using Ecom.CategoryService.Models;
using MediatR;
using System.Collections.Generic;

namespace Ecom.CategoryService.Queries
{
    public class GetCategoryQuery:IRequest<IEnumerable<EcomCategory>>
    {
        /*
            No input 
        */
    }
}

﻿using Ecom.CategoryService.BusinessLayer.Interface;
using Ecom.CategoryService.Models;
using Ecom.CategoryService.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.CategoryService.Handlers
{
    public class GetCategoryHandler : IRequestHandler<GetCategoryQuery, IEnumerable<EcomCategory>>
    {
        ICategory _data;

        public GetCategoryHandler(ICategory data)
        {
            _data = data;
        }

        public async Task<IEnumerable<EcomCategory>> Handle(GetCategoryQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetAllCategory());

        }
    }
}

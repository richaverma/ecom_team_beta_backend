﻿using Ecom.CategoryService.BusinessLayer.Interface;
using Ecom.CategoryService.Commands;
using Ecom.CategoryService.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.CategoryService.Handlers
{
    public class EditCategoryHandler : IRequestHandler<EditCategoryCommand, EcomCategory>
    {
        ICategory _data;

        public EditCategoryHandler(ICategory data)
        {
            _data = data;
        }
        public async Task<EcomCategory> Handle(EditCategoryCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.EditById(request.id, request.newCategoryName));
        }
    }
}

﻿using Ecom.CategoryService.BusinessLayer.Interface;
using Ecom.CategoryService.Commands;
using Ecom.CategoryService.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.CategoryService.Handlers
{
    public class AddCategoryHandler : IRequestHandler<AddNewCategoryCommand, IEnumerable<EcomCategory>>
    {
        ICategory _data;

        public AddCategoryHandler(ICategory data)
        {
            _data = data;
        }

        public async Task<IEnumerable<EcomCategory>> Handle(AddNewCategoryCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.AddNewCategory(request.EcomCategory));
        }
    }
}

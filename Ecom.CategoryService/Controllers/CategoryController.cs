﻿using Ecom.CategoryService.Commands;
using Ecom.CategoryService.Handlers;
using Ecom.CategoryService.Logger;
using Ecom.CategoryService.Models;
using Ecom.CategoryService.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecom.CategoryService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {

        private readonly IMediator mediator;
        private ILoggerService logger;

        public CategoryController(IMediator mediator  , ILoggerService logger)
        {
            this.mediator = mediator;
            this.logger = logger;
        }

        [HttpGet]
        [Route("get-all-category")]
        public async Task<IActionResult> GetCategory()
        {
            logger.LogInfo("Fetching all categories");
            var data = await mediator.Send(new GetCategoryQuery());
            return Ok(data);

        }

        [HttpPost]
        [Route("add-new-category")]
        public async Task<IActionResult> AddCategory([FromBody] EcomCategory newCategory)
        {
            logger.LogInfo($"Adding new category:{newCategory}");
            var data = await mediator.Send(new AddNewCategoryCommand() { EcomCategory = newCategory });
            return Ok(data);

        }

        [HttpPut]
        [Route("edit-category")]
        public async Task<IActionResult> EditCategory(int id, string newCategoryName)
        {
            logger.LogInfo("Editing a Category");
            var data = await mediator.Send(new EditCategoryCommand() { id = id, newCategoryName = newCategoryName });
            return Ok(data);
        }
    }
}

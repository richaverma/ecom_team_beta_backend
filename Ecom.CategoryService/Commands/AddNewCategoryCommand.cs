﻿using Ecom.CategoryService.Models;
using MediatR;
using System.Collections.Generic;

namespace Ecom.CategoryService.Commands
{
    public class AddNewCategoryCommand:IRequest<IEnumerable<EcomCategory>>
    {
        public EcomCategory EcomCategory { get; set; }
    }
}

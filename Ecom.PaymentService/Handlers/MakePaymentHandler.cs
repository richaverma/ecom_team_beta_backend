﻿using Ecom.PaymentService.BusinessLayer.Interface;
using Ecom.PaymentService.Commands;
using Ecom.PaymentService.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Ecom.PaymentService.Handlers
{
    public class MakePaymentHandler : IRequestHandler<MakePaymentCommand, EcomPayment>
    {
        IPayment payment;

        public MakePaymentHandler(IPayment payment)
        {
            this.payment = payment;
        }

        public async Task<EcomPayment> Handle(MakePaymentCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(payment.MakePayment(request.Payment));
        }
    }
}

﻿using Ecom.PaymentService.Models;
using MediatR;

namespace Ecom.PaymentService.Commands
{
    public class MakePaymentCommand :IRequest<EcomPayment>
    {
        public EcomPayment Payment { get; set; }
    }
}

﻿using Ecom.PaymentService.BusinessLayer.Interface;
using Ecom.PaymentService.Models;

namespace Ecom.PaymentService.BusinessLayer.Services
{
    public class PaymentServices : IPayment
    {
        private EcomContext _context;

        public PaymentServices(EcomContext ecomContext)
        {
            _context = ecomContext;
        }
        public EcomPayment MakePayment(EcomPayment newPayment)
        {

            _context.EcomPayment.Add(newPayment);
            _context.SaveChanges();
            return newPayment;
        }
    }
}

﻿using Ecom.CategoryService.Models;
using Ecom.CustomerService.BusinessLayer.Interface;
using Ecom.CustomerService.BusinessLayer.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Customer.WebService.Test.Mocks
{
    public static class MockRepository
    {
        public static Mock<ICustomer> GetCustomerService()
        {
            var cuslist = new List<EcomCustomers>
            {
                new EcomCustomers
                {
                    CustomerId = 1,
                    CustomerName = "Sharan",
                    CustomerAddress = "Banglore",
                    CustomerPhoneNumber = "1234567890",
                    CustomerEmailId = "sharan@gmail.com",
                    LoginId = 101
                },
                new EcomCustomers
                {
                    CustomerId = 2,
                    CustomerName = "Harsh",
                    CustomerAddress = "Banglore",
                    CustomerPhoneNumber = "1234567890",
                    CustomerEmailId = "harsh@gmail.com",
                    LoginId = 102
                }
            };

            var mockrepo = new Mock<ICustomer>();

            mockrepo.Setup(r => r.GetCustomerById(101)).ReturnsAsync(cuslist.SingleOrDefault(x => x.LoginId == 101));

            return mockrepo;
        }
    }
}

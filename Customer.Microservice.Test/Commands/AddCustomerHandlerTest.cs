﻿using Ecom.CategoryService.Models;
using Ecom.CustomerService.BusinessLayer.Interface;
using Ecom.CustomerService.BusinessLayer.Services;
using Ecom.CustomerService.Commands;
using Ecom.CustomerService.Handlers;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Customer.WebService.Test.Commands
{
    public class AddCustomerHandlerTest
    {
        private readonly Mock<ICustomer> _mockRepo;
        private readonly EcomCustomers _customer;

        public AddCustomerHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetCustomerService();

            _customer = new EcomCustomers
            {
                CustomerId = 3,
                CustomerName = "Vishal",
                CustomerAddress = "Banglore",
                CustomerPhoneNumber = "1234567890",
                CustomerEmailId = "vishal@gmail.com",
                LoginId = 103
            };
        }

        [Fact]
        public async Task AddCustomerTest()
        {
            var handler = new AddNewCustomerHandler(_mockRepo.Object);

            var result = handler.Handle(new AddNewCustomerCommand(), CancellationToken.None);
        }
    }
}

﻿namespace Ecom.ApiGateway.DTO
{
    public class CustomerDTO
    {
        public int CustomerId { get; set; }
        public int LoginId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerEmailId { get; set; }
    }
}

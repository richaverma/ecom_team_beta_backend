﻿using Ecom.ApiGateway.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Ecom.ApiGateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiGatewayController : ControllerBase
    {

        //Category
        /*
         1)Edit category
        */
        [HttpGet]
        [Route("get-all-category")] //Working
        public async Task<IActionResult> GetAllCategory()
        {
            var url = "https://localhost:44328/api/Category/get-all-category";
            using var client = new HttpClient();
            var content = await client.GetFromJsonAsync<IEnumerable<CategoryDTO>>(url);
            return Ok(content);
        }

        [HttpPost]
        [Route("add-new-category")] //Working
        public async Task<IActionResult> PostNewCategory([FromBody] CategoryDTO category)
        {
            var url = "https://localhost:44328/api/Category/add-new-category";
            using var client = new HttpClient();
            var content = await client.PostAsJsonAsync(url, category);
            return Ok(content.Content.ReadFromJsonAsync<IEnumerable<CategoryDTO>>().Result);
        }

        //Product
        /*
         1)Delete Product
        */

        [HttpPost]
        [Route("get-product-by-cat-id")] //Working
        public async Task<IActionResult> GetProductByCatId(int id)
        {
            var url = "https://localhost:44362/api/Product/get-product-by-cat-id/" + id;
            using var client = new HttpClient();
            var content = await client.GetFromJsonAsync<IEnumerable<ProductDTO>>(url);
            return Ok(content);

        }


        [HttpPost]
        [Route("get-product-by-id")] //Working
        public async Task<IActionResult> GetProductById(int id)
        {
            var url = "https://localhost:44362/api/Product/get-product-by-id/" + id;
            using var client = new HttpClient();
            var content = await client.GetFromJsonAsync<ProductDTO>(url);
            return Ok(content);

        }

        [HttpGet]
        [Route("get-all-products")] //Working
        public async Task<IActionResult> GetAllProducts()
        {
            var url = "https://localhost:44362/api/Product/get-all-products";
            using var client = new HttpClient();
            var content = await client.GetFromJsonAsync<IEnumerable<ProductDTO>>(url);
            return Ok(content);
        }

        [HttpPost]
        [Route("add-new-product")] //Working
        public async Task<IActionResult> AddNewProduct([FromBody] ProductDTO product)
        {
            var url = "https://localhost:44362/api/Product/add-new-product";
            using var client = new HttpClient();
            var content = await client.PostAsJsonAsync(url, product);
            return Ok(content.Content.ReadFromJsonAsync<IEnumerable<ProductDTO>>().Result);
        }

        [HttpPost]
        [Route("edit-product")] //Working
        public async Task<IActionResult> EditProduct([FromBody] ProductDTO product)
        {
            var url = "https://localhost:44362/api/Product/edit-product";
            using var client = new HttpClient();
            var content = await client.PutAsJsonAsync(url, product);
            return Ok(content.Content.ReadFromJsonAsync<ProductDTO>().Result);
        }

        //Payment
        [HttpPost]
        [Route("make-payment")] //Working
        public async Task<IActionResult> MakePayment([FromBody] PaymentDTO payment)
        {
            var url = "https://localhost:44365/api/Payment/make-payment";
            using var client = new HttpClient();
            var content = await client.PostAsJsonAsync(url, payment);
            return Ok(content.Content.ReadFromJsonAsync<PaymentDTO>().Result);
        }

        //Orders
        [HttpPost]
        [Route("get-order-by-id")] //Working
        public async Task<IActionResult> GetOrderById(int id)
        {
            var url = "https://localhost:44376/api/Order/get-order-by-id/" + id;
            using var client = new HttpClient();
            var content = await client.GetFromJsonAsync<OrdersDTO>(url);
            return Ok(content);

        }

        [HttpPost]
        [Route("place-order")] //Working
        public async Task<IActionResult> PlaceOrder([FromBody] OrdersDTO orders)
        {
            var url = "https://localhost:44376/api/Order/place-order";
            using var client = new HttpClient();
            var content = await client.PostAsJsonAsync(url, orders);
            return Ok(content.Content.ReadFromJsonAsync<OrdersDTO>().Result);
        }

        //Login
        [HttpPost]
        [Route("get-logindetail-id")] //Working
        public async Task<IActionResult> GetLoginById(int id)
        {
            var url = "https://localhost:44371/api/Login/get-login-id/" + id;
            using var client = new HttpClient();
            var content = await client.GetFromJsonAsync<LoginDTO>(url);
            return Ok(content);
        }

        [HttpPost]
        [Route("get-logindetail-email/{email}")] //Working
        public async Task<IActionResult> GetLoginByCustomerEmail(string email)
        {
            var url = "https://localhost:44371/api/Login/get-login-details/" + email;
            using var client = new HttpClient();
            var content = await client.GetFromJsonAsync<LoginDTO>(url);
             return Ok(content);
        }

        [HttpPost]
        [Route("new-login")]  //Working
        public async Task<IActionResult> AddNewLogin([FromBody] LoginDTO login)
        {
            var url = "https://localhost:44371/api/Login/new-login";
            using var client = new HttpClient();
            var content = await client.PostAsJsonAsync(url, login);
            return Ok(content.Content.ReadFromJsonAsync<LoginDTO>().Result);
        }

        //Customer
        [HttpPost]
        [Route("add-new-customer")] //Working
        public async Task<IActionResult> AddNewCustomer(CustomerDTO customer)
        {
            var url = "https://localhost:44344/api/Customer/add-new-customer";
            using var client = new HttpClient();
            var content = await client.PostAsJsonAsync(url, customer);
            return Ok(content.Content.ReadFromJsonAsync<CustomerDTO>().Result);
        }

        [HttpPost]
        [Route("get-customer-by-id/{id}")] //Working
        public async Task<IActionResult> GetCustomerById(int id)
        {
            var url = "https://localhost:44344/api/Customer/get-customer-by-id/" + id;
            using var client = new HttpClient();
            var content = await client.GetFromJsonAsync<CustomerDTO>(url);
            return Ok(content);
        }
    }
}

using Ecom.OrderService.BusinessLayer.Services;
using Ecom.OrderService;
using Moq;
using System;
using Xunit;
using Ecom.OrderService.BusinessLayer.Interface;

namespace Ecom_Test
{
    public class OrderTest
    {
        private readonly AddOrderRequest _request;
        private readonly Mock<IOrder> _addorderRepositoryMock;
        private readonly AddOrderRequestProcessor _processor;
        //private object _addorderRepositoryMock;
        public OrderTest()
        {
            _request = new AddOrderRequest
            {
                OrderId = 700,
                ProductId = 500,
                CustomerId = 0,
                OrderQuantity = 0,
                OrderPrice = 0,
                ShipmentAddress = null

            };
            _addorderRepositoryMock = new Mock<IOrder>();
            _processor = new AddOrderRequestProcessor(_addorderRepositoryMock.Object);
        }


        [Fact]
        public void ShouldReturnOrderResultWithRequestValues()
        {
            // Act
            AddOrderResponse response = _processor.Add(_request);

            // Assert
            Assert.NotNull(response);
            Assert.Equal(_request.OrderId, response.OrderId);

        }

        [Fact]
        public void ShouldThrowExceptionIfRequestIsNull()
        {
            // Act
            var exception = Assert.Throws<ArgumentNullException>(() => _processor.Add(null));

            // Assert
            Assert.Equal("request", exception.ParamName);
        }

    }

}
